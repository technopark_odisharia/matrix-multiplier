package Local::MatrixMultiplier;

use strict;
use warnings;

sub mult {
    my ($matrix_a, $matrix_b) = @_;
    my ($array_a, $array_b) = (serialize_matrix($matrix_a), serialize_matrix($matrix_b));
    my ($size_matrix_a, $size_array_a) = (scalar @{$matrix_a}, scalar @{$array_a});
    my $res = [];

    check($matrix_a, $matrix_b);
    my $max_child = ( $_[2] > $size_array_a ) ? $size_array_a : $_[2];
    # We left interval as float number because it leads to better dividing work for childs
    my $interval = $size_array_a / $max_child;

    for my $i (0..$max_child - 1) {
        my ($r, $w);
        pipe ($r, $w);

        defined(my $chld = fork()) or die "fork: $!";
        if ($chld) {
            close $w;

            my $start_fork = $interval * $i;
            $res->[$start_fork++] = 0 + $_ while (<$r>);

            close $r;
        } else {
            close $r;

            my $start = $interval * $i;
            my $end = ($i == $max_child - 1) ? $size_array_a - 1 : ($i + 1) * $interval - 1;
            calc($res, $array_a, $array_b, $size_matrix_a, $start, $end);
            print $w "$res->[$_]\n" foreach ($start .. $end);

            close $w;
            exit;
        }
    }
    return matrixize($res, $size_matrix_a);
}

sub calc {
    my ($res, $array_a, $array_b, $size, $start, $end) = @_;
    for my $index ($start .. $end) {
        $res->[$index] = 0;
        my $i_index = int ($index / $size);
        my $j_index = $index % $size;
        $res->[$index] += $array_a->[$i_index * $size + $_]
                        * $array_b->[$_ * $size + $j_index] foreach (0 .. $size - 1);
    }
}

sub check {
    my ($mat_a, $mat_b) = @_;
    die if scalar @{$mat_a} == 0
        || scalar @{$mat_b} == 0
        || scalar @{$mat_a} != scalar @{$mat_b};
    for my $matrix ($mat_a, $mat_b) {
        for my $string (@{$matrix}) {
            die if scalar @{$string} != scalar @{$matrix};
        }
    }
}

sub serialize_matrix {
    my $matrix = shift;
    my @array;
    push @array, @{$_} foreach (@{$matrix});
    return \@array;
}

sub matrixize {
    my ($array, $size) = @_;
    my @matrix;
    @{$matrix[$_]} = splice(@{$array}, 0, $size) foreach (0 .. $size - 1);
    return \@matrix;
}

1;
